<html>
<head>
    <title>Laravel Skills Test</title>
    
    <!-- reference: https://getbootstrap.com/ --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="container">
    <div class="form-container row">
        <div class="column col-4 offset-4 mt-5 text-center">
            <form method="post" action="/products" id="product_form">
                <input type="text" class="form-control" name="product_name" placeholder="Product name">
                <input type="number" class="form-control" name="quantity" placeholder="Quantity in stock">
                <input type="number" class="form-control" name="price" placeholder="Price per item">
                <button type="submit" class="form-control btn btn-primary">Add Product</button>
            </form>
        </div>
    </div>
    <div class="list-container row">
        <div class="column col-8 offset-2 mt-5 text-center">
            <table class="table" id="product_table">
                <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Quantity in stock</th>
                        <th>Price per item</th>
                        <th>Datetime Submitted</th>
                    </tr>
                </thead>
                <tbody>
                    @if($products)
                        @foreach($products as $product)
                        <tr>
                            <td>{{$product->product_name}}</td>
                            <td>{{$product->quantity}}</td>
                            <td>{{$product->price}}</td>
                            <td>{{$product->datetime}}</td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/product.js') }}"></script>
</body>
</html>