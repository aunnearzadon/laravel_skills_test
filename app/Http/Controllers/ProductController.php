<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    public function index(){
        $products = File::get('products.json');
        //dd(json_decode($products));
        return view("products")->with('products', json_decode($products));
    }

    public function addProduct(){
        $products = json_decode(File::get('products.json'));
        $data = Input::all();

        $product = new Product;
        $product->product_name = $data['product_name'];
        $product->quantity = $data['quantity'];
        $product->price = $data['price'];
        $product->datetime = date("F j, Y, g:i a");
        
        //reference https://laracasts.com/discuss/channels/general-discussion/saving-json-to-file-decoding
        $products[]= $product;
        File::put('products.json', json_encode($products));
        return json_encode($product);
    }
}