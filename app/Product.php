<?php

namespace App;

class Product
{
    public $product_name;
    public $quantity;
    public $price;
    public $datetime;
}