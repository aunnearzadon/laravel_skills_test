//reference https://laravel.com/docs/5.5/csrf
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#product_form").on('submit', function(e){
    e.preventDefault();
    var data = {
        product_name: $("input[name=product_name]").val(),
        quantity: $("input[name=quantity]").val(),
        price: $("input[name=price]").val()
    }

    $.post('/products', data, function(response){
        var response = $.parseJSON(response)
        var row = "<tr><td>" 
            + response.product_name 
            + "</td><td>" 
            + response.quantity 
            + "</td><td>" 
            + response.price 
            + "</td><td>" 
            + response.datetime
            + "</td></tr>"
        $("#product_table tbody").append(row);
    });
});